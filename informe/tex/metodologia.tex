\section{Preprocesamiento de datos}
\label{sec:metodologia}
Se realizó el análisis sobre una base de datos que contiene \emph{rankings}
semanales de la plataforma \emph{Spotify} mediante el sitio
\emph{SpotifyCharts}\cite{Spotifycharts}. Sobre ésta se integraron datos de
la misma plataforma que describen características y métricas de las canciones
(\emph{audio_features}). Adicionalmente, se integraron las letras de algunas
canciones tomadas desde el sitio \emph{Genius Lyrics}\cite{genius}.
Las bases de datos fueron provistas por la cátedra en archivos en formato
\emph{JSON} para su uso en el gestor de bases de datos \emph{MongoDB}.

\subsection{Limpieza de datos}
Cada \emph{dataset} se limpió y acondicionó por separado a fin de lograr una
mejor integración. Se describe cada uno de ellos y las operaciones
realizadas.

\subsubsection{Charts}
Este dataset contiene las posiciones de cada semana durante el tiempo
estudiado de 3 años (2018, 2019, 2020 y parte de 2021). La tabla cuenta
con: \emph{Position}, \emph{Track_Name}, \emph{Artist},
\emph{Streams}, \emph{URL}, \emph{week_start} y \emph{week_end}.
Consta de 63600 registros de los cuales no se detectaron faltantes pero sí
 duplicados. Se adoptó como criterio de unicidad la tupla:
\emph{URL}, \emph{Position}, \emph{week_start}, es decir que para la misma
canción en la misma semana corresponde un único puesto. Descartando los registros
duplicados se obtienen 31400 registros únicos.

\subsubsection{Audio_features}
Este \emph{dataset} contiene métricas y características de las canciones según se
describen en la \emph{API Web de Spotify}\cite{spotify}. Los atributos que
se utilizan son: \emph{external_url_spotify}, \emph{track_name}, \emph{duration},
\emph{artist_name}, \emph{danceability}, \emph{loudness}, \emph{energy},
\emph{speechiness}, \emph{valence}, \emph{instrumentalness},
\emph{acousticness}, \emph{liveliness}, \emph{tempo}. El resto de los
atributos del \emph{dataset} se descartaron para el presente análisis:
\emph{album_id}, \emph{album_images}, \emph{album_name},
\emph{album_release_date}, \emph{album_release_date_precision},
\emph{album_release_year}, \emph{album_type}, \emph{analisys_url},
\emph{artist_id}, \emph{available_markets}, \emph{disc_number},
\emph{explicit}, \emph{is_local}, \emph{key}, \emph{key_mode},
\emph{time_signature}, \emph{track_href}, \emph{track_id},
\emph{track_number}, \emph{track_preview_url}, \emph{track_uri},
\emph{type}.

Se detectó la presencia de valores faltantes y de registros duplicados. Se
adoptó como criterio de unicidad la \emph{external_url_spotify} para cada
canción que a la vez se cambió su nombre a \emph{URL} y utilizó como
identificador en la integración con el
dataset \emph{Charts}. En los casos en los cuales la \emph{URL} no resulte
única pero su diferencia se deba a que existe más de un artista
relacionado, se incluyó
esa información como una característica dicotómica adicional
(\emph{is_en_grupo}). Luego
de eliminar los duplicados se obtuvieron 411500 registros.

\subsubsection{Lyrics}
Este \emph{dataset} contiene letras de canciones extraídas del sitio \emph{Genius
Lyrics}\cite{genius}. Se trata de 6334 canciones con las variables: \emph{artist_name},
\emph{track_name} y \emph{lyrics}, esta última conteniendo la letra
completa de la canción y parte de texto del sitio. Se detectaron duplicados
entre las canciones que fueron descartados usando el criterio de unicidad
de la tupla \emph{artist_name} y \emph{track_name} obteniendo 1361
registros únicos. Esa misma tupla se utilizó para integrar esta información
con el dataset \emph{Chart}.

\subsection{Preprocesamiento}
Los tres \emph{datasets} anteriores se integraron con el fin de
obtener una vista minable acorde al método propuesto de minería de datos
basado en el paquete del lenguaje de programación R
\emph{arules}\cite{arules}.

\subsubsection{Rankings semanales (\emph{charts})}
Se utilizó para cada canción un indicador de su máximo puesto alcanzado
(mínima \emph{Position}) y se lo discretizó en la variable \emph{level_pop}
formando tres categorías: para los primeros 30 puestos (\emph{top_30}), para los puestos
entre 31 y 100 (\emph{top_100}) y los puestos entre 101 y 200
(\emph{top_200}).

Simultáneamente, a cada canción se le asignó una categoría de acuerdo a la
cantidad de semanas (sean consecutivas o no) de aparición en los
\emph{rankings} creando la variable \emph{level_tiempo}: para las que
aparecen hasta 2 veces (\emph{poco}), los que aparecen entre 3 y 10 semanas
(\emph{promedio}) y las que aparecen más de 10 semanas (\emph{mucho}). A
esta característica la llamamos \emph{permanencia}.

\subsubsection{Características de canciones (\emph{audio_features})}
Las características numéricas \emph{instrumentalness}, \emph{liveliness},
\emph{acousticness}, \emph{danceability}, \emph{speechiness} se
discretizaron en función de las recomendaciones de \emph{Spotify} en su
\emph{API Web}\cite{spotify}. El resto de las variables numéricas (\emph{loudness},
\emph{tempo}, \emph{valence}, \emph{energy}, \emph{duration}) se
discretizaron adoptando criterios estadísticos de su distribución o
conocimiento de negocio en el caso de \emph{loudness} y \emph{tempo}.

Las características con discretización booleana se prefijaron con
\emph{is_} (e.g. \emph{is_loud}) mientras que las que resultan en niveles
se prefijaron con \emph{level_} (e.g. \emph{level_speech}). Una vez hecha
la discretización se descartaron las variables numéricas del dataset.

\subsubsection{Letras de canciones (\emph{lyrics_dm})}
Las letras de canciones se seleccionaron primariamente por su idioma
(eligiendo el inglés por ser en el que mejor funcionan los paquetes de
minería de texto). Para esta selección se utilizó el paquete R
\emph{textcat}\cite{textcat}. Luego, se efectuó una limpieza de los textos
corriendo rutinas del paquete R \emph{tm}\cite{tm}. Se eliminó el texto
cabecera y pie del sitio web, se convirtió todo el texto a minúsculas, se
eliminó todo signo de puntuación (es decir, se utilizaron solamente
caracteres alfanuméricos), se eliminaron todos los dígitos y tildes y luego
se eliminaron las palabras vacías del lenguaje elegido (inglés).

Con las
letras preprocesadas generamos un corpus de documentos del cual se extrajo
una matriz de término-documento con los 10000 términos más frecuentes en el
corpus utilizando la función de peso \emph{weightTfIdf} (peso proporcional
a la inversa de la frecuencia).

Sobre las letras se generó un indicador de "positividad" utilizando el
paquete R \emph{sentimentr}\cite{sentimentr} que asigna un nivel de
emoción positiva o negativa a frases completas. El valor de positividad de
cada letra es un promedio ponderado del valor asignado a cada una de sus
frases. Luego se discretizó el campo (\emph{level_positividad}) con valores
de emoción \emph{positiva}, \emph{neutra}, \emph{negativa}.

\subsection{Integración de los datos}
Se integraron los tres datasets utilizando \emph{URL} para relacionar
\emph{Audio_features} con \emph{Charts} y la tupla \emph{artist_name} y
\emph{track_name} para agregar las letras de las canciones contenidas en la
tabla \emph{lyrics_dm}. Se eliminaron los registros que no tuviesen algún
correlato en una de las tablas. De la relación resultó un
total de 439 canciones dentro del \emph{ranking} en idioma inglés que
contienen letra y sus características auditivas.

Se muestran las variables utilizadas y sus intervalos de discretización en
la Tabla~\ref{tbl:variables} y su distribución en el \emph{dataset} final
en la Figura~\ref{fig:distribuciones}. Se observó que las variables
\emph{is_danceable}, \emph{is_loud}, \emph{is_acoustic}, \emph{is_lively},
\emph{is_instrumental} y \emph{level_speech} se encuentran fuertemente
desbalanceadas en el \emph{subset} de datos manejado.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.48\textwidth]{fig/figura_1}
    \includegraphics[width=0.48\textwidth]{fig/figura_2}
    \caption{Distribución de variables en el \emph{dataset}}
    \label{fig:distribuciones}
\end{figure}

\begin{table}
    \tiny
    \centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{|l|l|l|}
    \hline
Nombre Variable   & Descripcion
& Valores
\\ \hline
is_en_grupo       & Define si el cantante es unico                                                                                                                                        & \begin{tabular}[c]{@{}l@{}}TRUE = varios cantantes\\ FALSE= un cantante\end{tabular}                                                               \\ \hline
is_instrumental   & Predice si la cancion contiene palabras                                                                                                                               & \begin{tabular}[c]{@{}l@{}}TRUE = No contiene palabras (> 0.5 instrumentalness)\\ FALSE= contiene palabras (<0.5 instrumentalness)\end{tabular}    \\ \hline
is_lively         & Detecta la presencia de publico                                                                                                                                       & \begin{tabular}[c]{@{}l@{}}TRUE = liveness > 0.8, hay publico presente.\\ FALSE = lveness < 0.8 no hay publico presente.\end{tabular}              \\ \hline
is_acoustic       & Determina si la cancion es acustica                                                                                                                                   & \begin{tabular}[c]{@{}l@{}}TRUE = acousticness > 0.8, es acustica.\\ FALSE = acousticness <0.8, no es acustica.\end{tabular}                       \\ \hline
is_danceable      & \begin{tabular}[c]{@{}l@{}}Determina si una cancion es bailable basado en otras \\ caracteristicas.\end{tabular}                                                      & \begin{tabular}[c]{@{}l@{}}TRUE = danceability >0.5, es bailable.\\ FALSE = danceability <0.5 no es bailable.\end{tabular}                         \\ \hline
is_loud           & Determina si la cancion es Ruidosa. (medido en dB)                                                                                                                    & \begin{tabular}[c]{@{}l@{}}TRUE= loudness >-9 dB, es ruidosa.\\ FALSE= loudness < -9 dB, no es ruidosa.\end{tabular}                               \\ \hline
level_tempo       & Tempo medido en bpm.                                                                                                                                                  & \begin{tabular}[c]{@{}l@{}}"adagio" = tempo < 80 bpm\\ "moderato" = tempo 81-110 bpm\\ "allegro" = 111-170 bpm\\ "presto" = > 170 bpm\end{tabular} \\ \hline
level_valence     & Positividad de la cancion referido a la musica                                                                                                                        & \begin{tabular}[c]{@{}l@{}}"down" = <0.33\\ "neutral" = 0.33-0.66\\ "cheerful" = > 0.66\end{tabular}                                               \\ \hline
level_energy      & Nivel intensidad y actividad                                                                                                                                          & \begin{tabular}[c]{@{}l@{}}"bajo" = <0.33\\ "medio" = 0.33-0.66\\ "alto"= >0.66\end{tabular}                                                       \\ \hline
level_speech      & Detecta la presencia de lenguaje hablado                                                                                                                              & \begin{tabular}[c]{@{}l@{}}"bajo" = <0.33\\ "medio" = 0.33-0.66\\ "alto" = > 0.66\end{tabular}                                                     \\ \hline
level_duration    & Duracion del tema en ms                                                                                                                                               & \begin{tabular}[c]{@{}l@{}}"baja" = < 170000 ms\\ "media" = 170000-220000 ms\\ "alta" => 220000 ms\end{tabular}                                    \\ \hline
level_pop         & Maximo puesto en el ranking alcanzado                                                                                                                                 & \begin{tabular}[c]{@{}l@{}}"top_30" = 1-30\\ "top_100" = 31-100\\ "top_200" = 101-200\end{tabular}                                                 \\ \hline
level_tiempo      & \begin{tabular}[c]{@{}l@{}}Cantidad de semanas en el Ranking (no necesariamente \\ continuas)\end{tabular}                                                            & \begin{tabular}[c]{@{}l@{}}"poco" = 1-2\\ "promedio" = 3-10\\ "mucho" = > 10\end{tabular}                                                          \\ \hline
level_positividad & \begin{tabular}[c]{@{}l@{}}Define la positividad de la letra de la cancion en base al \\ valor promedio obtenido del analisis con la funcion sentiment()\end{tabular} & \begin{tabular}[c]{@{}l@{}}"negativo" = -inf - (-0.05)\\ "neutro"= -0.05- 0.05\\ "positivo"= >0.05\end{tabular}                                    \\ \hline
TERM_xxxxx        & \begin{tabular}[c]{@{}l@{}}Una columna para cada
termino frecuente. Determina presencia o \\ ausencia de un termino en la
cancion.\end{tabular}                       & \begin{tabular}[c]{@{}l@{}}1
= presente\\ 0 = ausente\end{tabular}  \\\hline
\end{tabular}%
}


\caption{Discretización de variables}
\label{tbl:variables}
\end{table}

Luego este \emph{dataset} se convirtió a formato de transacciones
para poder ser utilizado por el paquete R \emph{arules}\cite{arules} que se
utilizó para obtener reglas de asociación. A esta vista se la denomina
\emph{transacciones} a lo largo del presente.

