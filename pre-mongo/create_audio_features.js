db.artist_audio_features.aggregate([
   {$project: {
        URL: "$external_urls_spotify",
        track_name: "$track_name",
        duration: "$duration_ms",
        artist_name: "$artist_name",
        danceability: "$danceability",
        loudness: "$loudness",
        energy: "$energy",
        speechiness: "$speechiness",
        valence: "$valence",
        instrumentalness: "$instrumentalness",
        acousticness: "$acousticness",
        liveliness: "$liveness",
        tempo: "$tempo"
       }
    },
    {$out: "audio_features"}
])
