db.charts.aggregate([
  {
    $group: { _id: { URL: "$URL", week_start: "$week_start", Position: "$Position"},
      doc: { $last: "$$ROOT" } // Retrieve only last doc in a group
    }
  },
  {
    $replaceRoot: { newRoot: "$doc" } // replace doc as object as new root of document
  },
  { $out : 'charts_new' } // Test above aggregation & then use this 
])