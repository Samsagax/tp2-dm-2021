#!/usr/bin/bash

# Recrear la base de datos leugo de importar en la instancia docker
USERNAME=dmuba
PASSWD=dmuba
DATABASE="DMUBA_SPOTIFY"

mongsh -u "$USERNAME" -p "$PASSWD" --authenticationDatabase admin \
    "localhost:27017/$DATABASE" remove_chart_duplicates.js
mongsh -u "$USERNAME" -p "$PASSWD" --authenticationDatabase admin \
    "localhost:27017/$DATABASE" create_artist_features.js
