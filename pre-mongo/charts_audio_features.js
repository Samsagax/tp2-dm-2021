db.artist_audio_features.aggregate([
    {$lookup:
        {from: "charts_new",
        localField: "external_urls_spotify",
        foreignField: "URL",
        as: "chr"}
    },
    {$unwind: "$chr"},
    {$project: {
        URL: "$chr.URL",
        track_name: "$track_name",
        duration: "$duration_ms",
        artist_name: "$artist_name",
        c_position: "$chr.Position",
        c_week_start: "$chr.week_start",
        f_danceability: "$danceability",
        f_loudness: "$loudness",
        f_energy: "$energy",
        f_speechiness: "$speechiness",
        f_liveness: "$liveness"}
    },
    {$out: "chart_audio_features"}
])

