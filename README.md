# TP de Minería de datos

Integrantes:
- Diego Halac
- Julian Kligman
- Joaquín Aramendía

# Guía de supervivencia

A continuación unos pequeños ayudamemoria

## Uso de git

### Flujo de trabajo básico

- Incorporar cambios desde la nube (lo que trabajaron otros):
```
$ git pull origin master
```

- Editar con las herramientas que se quieran (por ejemplo RStudio, VSCode)

- Revisar cambios
```
$ git status
$ git diff
```

- Commit de los cambios:
```
$ git commit -a -m "Mensaje corto"
```
Alternativamente se puede omitir el argumento `-m`. Git abrirá un editor de
texto configurado y permitirá hacer un mensaje multilinea

- Agregar los cambios a la nube
```
$ git push origin master
```

## Correr mongodb

Desde la carpeta `docker-mongo` usar `docker-compose`

```
$ docker-compose up
```

para ingresar usar las credenciales
```
user: dmuba
password: dmuba
```

## Importar los datos

Usando `mongoimport` sobre la instancia docker
```
$ mongoimport --db DMUBA_SPOTIFY -u dmuba -p dmuba --authenticationDatabase admin --drop --collection charts --file charts-dm.json
$ mongoimport --db DMUBA_SPOTIFY -u dmuba -p dmuba --authenticationDatabase admin --drop --collection artist --file artist-dm.json
$ mongoimport --db DMUBA_SPOTIFY -u dmuba -p dmuba --authenticationDatabase admin --drop --collection artist_audio_features --file artist_audio_features-dm.json

```
